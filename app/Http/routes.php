<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', ['as' => 'main', 'uses' => 'IndexController@index']);
Route::get('projects', ['as' => 'projectlist', 'uses' => 'IndexController@projectlist']);
Route::get('projects/{slug}', ['as' => 'projectCart', 'uses' => 'IndexController@projectCart']);


Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);


//For tests
Route::get('/test', ['as' => 'tast', 'uses' => 'TestController@index']);

