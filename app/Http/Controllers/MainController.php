<?php
/**
 * Created by PhpStorm.
 * User: ОБИ ВАН
 * Date: 29.03.2017
 * Time: 0:17
 */

namespace App\Http\Controllers;


use App\Menu;

class MainController extends Controller
{
    public function __construct(Menu $menuModel)
    {
        $this->data = [];

        $this->data['menu']['left'] = $menuModel->getLeftMenu();
        $this->data['menu']['right'] = $menuModel->getRightMenu();
    }
}