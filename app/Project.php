<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model {

	protected $table = 'projects';

    public function galleries()
    {
        return $this->hasMany('App\Gallery');
    }

    public function getActive()
    {
        return $this->where(['active'=>1])
            ->get();
    }

    public function getBySlug($slug)
    {
        return $this
            ->where(['active'=>1])
            ->where(['slug'=>$slug])
            ->firstOrFail();
    }

}
