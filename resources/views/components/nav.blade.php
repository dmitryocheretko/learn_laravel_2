<dvi class="row">

    <!--left menu-->
    <div class="col-md-5">
        @foreach($menu['left'] as $item)
            <a href="{{$item->url}}">{{$item->title}}</a>
        @endforeach
    </div>

    <!--logo-->
    <div class="col-md-2">Logo</div>

    <!--right menu-->
    <div class="col-md-5">
        @foreach($menu['right'] as $item)
            <a href="{{$item->url}}">{{$item->title}}</a>
        @endforeach
    </div>

</dvi>